  

    ____  _____  ____   ______ ______        ______ ____   ____ 
   / __ \|__  / / __ ) / ____// ____/       / ____// __ \ / __ \
  / / / / /_ < / __  |/ /_   / / __ ______ / /_   / /_/ // / / /
 / /_/ /___/ // /_/ // __/  / /_/ //_____// __/  / ____// /_/ / 
/_____//____//_____//_/     \____/       /_/    /_/    /_____/
                                                             


________________________________________________________________


This is a modified version of RBDOOM-3-BFG engine. You can find all needed info about compilation, etc. at the following links:

RBDOOM-3-BFG Original Readme - https://raw.githubusercontent.com/RobertBeckebans/RBDOOM-3-BFG/master/README.txt
RBDOOM-3-BFG Release Notes - https://raw.githubusercontent.com/RobertBeckebans/RBDOOM-3-BFG/master/RELEASE-NOTES.txt

Compatible and tested FFmpeg DLLs are included in the folder with same name. 
This version is mainly targeted towards Visual Studio 2017. There are other scripts for building solutions included for other platforms, but their functionality was not fully tested.

Thank you for downloading D3BFG-FPD!

Logo generated with patorjk.com ( http://patorjk.com/software/taag/#p=display&h=1&v=1&f=Slant&t=D3BFG-FPD )
