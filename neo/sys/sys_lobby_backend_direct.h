/*
===========================================================================

Doom 3 BFG Edition GPL Source Code
Copyright (C) 1993-2012 id Software LLC, a ZeniMax Media company.

This file is part of the Doom 3 BFG Edition GPL Source Code ("Doom 3 BFG Edition Source Code").

Doom 3 BFG Edition Source Code is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Doom 3 BFG Edition Source Code is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Doom 3 BFG Edition Source Code.  If not, see <http://www.gnu.org/licenses/>.

In addition, the Doom 3 BFG Edition Source Code is also subject to certain additional terms. You should have received a copy of these additional terms immediately following the terms and conditions of the GNU General Public License which accompanied the Doom 3 BFG Edition Source Code.  If not, please request a copy in writing from id Software at the address below.

If you have questions concerning this license or the applicable additional terms, you may contact in writing id Software LLC, c/o ZeniMax Media Inc., Suite 120, Rockville, Maryland 20850 USA.

===========================================================================
*/
#ifndef	__SYS_LOBBY_BACKEND_DIRECT_H__
#define	__SYS_LOBBY_BACKEND_DIRECT_H__

/*
========================
idLobbyBackendDirect
========================
*/
class idLobbyBackendDirect : public idLobbyBackend
{
public:
	idLobbyBackendDirect();
	
	// idLobbyBackend interface
	void				StartHosting( const idMatchParameters& p, float skillLevel, lobbyBackendType_t type ) override;
	void				StartFinding( const idMatchParameters& p, int numPartyUsers, float skillLevel ) override;
	void				JoinFromConnectInfo( const lobbyConnectInfo_t& connectInfo ) override;
	void				GetSearchResults( idList< lobbyConnectInfo_t >& searchResults ) override;
	void				FillMsgWithPostConnectInfo( idBitMsg& msg ) override {}
	void				PostConnectFromMsg( idBitMsg& msg ) override {}
	void				Shutdown() override;
	void				GetOwnerAddress( lobbyAddress_t& outAddr ) override;
	void				SetIsJoinable( bool joinable ) override;
	lobbyConnectInfo_t	GetConnectInfo() override;
	bool				IsOwnerOfConnectInfo( const lobbyConnectInfo_t& connectInfo ) const override;
	void				Pump() override;
	void				UpdateMatchParms( const idMatchParameters& p ) override;
	void				UpdateLobbySkill( float lobbySkill ) override;
	void				SetInGame( bool value ) override;

	lobbyBackendState_t	GetState() override
	{
		return state;
	}

	void				BecomeHost( int numInvites ) override;
	void				FinishBecomeHost() override;

	void			RegisterUser( lobbyUser_t* user, bool isLocal ) override;
	void			UnregisterUser( lobbyUser_t* user, bool isLocal ) override;
	
private:

	lobbyBackendState_t		state;
	netadr_t				address;
};

#endif	// __SYS_LOBBY_BACKEND_DIRECT_H__ 
